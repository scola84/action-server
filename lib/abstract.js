'use strict';

const EventHandler = require('@scola/events');

class AbstractAction extends EventHandler {
  constructor(validator) {
    super();

    this.validator = validator;
    this.messenger = null;

    this.properties = {
      data: null,
      hash: null,
      name: null
    };

    this.connections = new Map();
  }

  destroy() {
    this.messenger.emit('debug', this, 'destroy');
    this.messenger.deleteAction(this);
  }

  getMessenger() {
    return this.messenger;
  }

  setMessenger(messenger) {
    this.messenger = messenger;
    return this;
  }

  getName() {
    return this.properties.name;
  }

  setName(name) {
    this.properties.name = name;
    return this;
  }

  getData() {
    return this.properties.data;
  }

  setData(data) {
    this.properties.data = data;
    return this;
  }

  getHash() {
    if (!this.properties.hash) {
      this.properties.hash = this.messenger
        .createHash(this.properties.name, this.properties.data);
    }

    return this.properties.hash;
  }

  setHash(hash) {
    this.properties.hash = hash;
    return this;
  }

  bind(connection, message) {
    this.messenger.emit('debug', this, 'bind', connection, message);
    this.connections.set(message.getConnection(), message);
  }

  unbind(connection) {
    this.messenger.emit('debug', this, 'unbind', connection);
    this.connections.delete(connection);

    if (this.connections.size === 0) {
      this.destroy();
    }
  }

  handleRequest(request, message) {
    this.messenger.emit('debug', this, 'handleRequest', request, message);

    return Promise.resolve()
      .then(this.authorize.bind(this, request.data, request, message))
      .then(this.validate.bind(this, request.data, request, message))
      .then(this.execute.bind(this, request.data, request, message));
  }

  authorize() {}

  validate() {}

  execute() {
    throw new Error('not_implemented');
  }
}

module.exports = AbstractAction;
