'use strict';

class Dispatcher {
  constructor(actions) {
    this.actions = actions;
  }

  get(name) {
    if (!this.actions[name]) {
      throw new Error('action_not_found', {
        detail: {
          name
        }
      });
    }

    return this.actions[name]
      .get();
  }
}

module.exports = Dispatcher;
