'use strict';

const ObjectHash = require('object-hash');
const Error = require('@scola/error');
const EventHandler = require('@scola/events');

class Messenger extends EventHandler {
  constructor(dispatcher) {
    super();

    this.dispatcher = dispatcher;
    this.transport = null;
    this.actions = new Map();
  }

  open(transport) {
    this.emit('debug', this, 'open', transport);

    this.transport = transport;
    this.bindListeners();

    return this;
  }

  close() {
    this.emit('debug', this, 'close');
    this.unbindListeners();

    return this;
  }

  send(message) {
    this.emit('debug', this, 'send', message);

    this.transport.send(message
      .delete('bind')
      .delete('name')
    );

    return this;
  }

  createHash(name, data) {
    this.emit('debug', this, 'createHash', name, data);

    return ObjectHash.sha1({
      name,
      data
    });
  }

  bindListeners() {
    this.bindListener('close', this.transport, this.handleClose);
    this.bindListener('message', this.transport, this.handleMessage);
  }

  unbindListeners() {
    this.unbindListener('close', this.transport, this.handleClose);
    this.unbindListener('message', this.transport, this.handleMessage);
  }

  handleClose(connection) {
    this.emit('debug', this, 'handleClose', connection);

    this.actions.forEach((action) => {
      action.unbind(connection);
    });
  }

  handleMessage(message) {
    this.emit('debug', this, 'handleMessage', message);

    try {
      this.handleRequest(message);
    } catch (error) {
      this.handleError(message, error);
    }
  }

  handleRequest(message) {
    this.emit('debug', this, 'handleRequest', message);

    const request = message.getBody();
    request.data = request.data || {};

    if (!request.name) {
      throw new Error('action_request_invalid');
    }

    const action = this.getAction(request, message);

    action
      .handleRequest(request, message)
      .then(this.handleBind.bind(this, action, request, message))
      .catch(this.handleError.bind(this, message));
  }

  handleBind(action, request, message) {
    this.emit('debug', this, 'handleBind', action, request, message);

    if (request.bind === true) {
      action.bind(message.getConnection(), message);

      if (!this.actions.has(action.getHash())) {
        this.actions.set(action.getHash(), action);
      }
    } else if (request.bind === false) {
      action.unbind(message.getConnection());
    }
  }

  handleError(message, error) {
    error = new Error('action_message_not_handled', {
      origin: error,
      detail: {
        message
      }
    });

    this.emit('error', error);

    this.send(message
      .clone()
      .setStatus(error.status)
      .set('data', {
        error
      })
    );
  }

  getAction(request, message) {
    this.emit('debug', this, 'getAction', request, message);

    const hash = this.createHash(request.name, request.data);
    let action = null;

    if (this.actions.has(hash)) {
      action = this.actions.get(hash);
    } else {
      action = this.dispatcher
        .get(request.name)
        .setMessenger(this)
        .setHash(hash)
        .setName(request.name)
        .setData(request.data);
    }

    return action;
  }

  deleteAction(action) {
    this.emit('debug', this, 'deleteAction', action);
    this.actions.delete(action.getHash());

    return this;
  }
}

module.exports = Messenger;
